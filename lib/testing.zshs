
#Prevents an error from making the test fail
function error_checked {
  (( errors-- )) || true #Prevent further error depending on result
}

function fail {
  test_status=$ZSHUNIT_TEST_FAILED
  error_checked #We track this as a failure, so we don't want the next call to count as an unchecked error
  error "$ZSHUNIT_FAIL_PREFIX" "$argv[@]"
}

#Create a mock for a function
function mock {
  assign "zshunit_${argv[1]}_mock_body" "$argv[2]"
  assign "zshunit_${argv[1]}_mock_status" "$argv[3]"
  assign "zshunit_${argv[1]}_mock_calls" 0

  function $argv[1] {
    local body="zshunit_${0}_mock_body"
    local st="zshunit_${0}_mock_status"
    local calls="zshunit_${0}_mock_calls"

    (( $calls++ )) || true #Prevent further error depending on result

    #if/else needed for correct number of errors according to body and return value
    if ! empty "${(P)st}"; then
      eval "${(P)body}"
      return "${(P)st}"
    else
      eval "${(P)body}"
    fi
  }
}

function check_mock_num_calls {
  local calls="zshunit_${argv[1]}_mock_calls"
  check_equal "expected to call '${argv[1]}' $argv[2] times but it was called ${(P)calls} times" "${(P)calls}" "$argv[2]"
}

def_option "ZSHUNIT_C_FAILED_DM" 'expected to fail but was fine'

function check_failed {
  local st=$?
  local msg="$ZSHUNIT_C_FAILED_DM"

  if [[ $# -gt 0 ]]; then
    msg="$argv[1]"
    shift
  fi

  if ! failed "${argv[1]:-$st}"; then
    fail "${(e)msg}"
  else
    (( errors-- )) || true #Prevent further error depending on result
  fi
}

def_option "ZSHUNIT_C_FAILED_AS_DM" 'expected to fail as <$argv[1]> but was <$st>'

function check_failed_as {
  local st=$?
  local msg="$ZSHUNIT_C_FAILED_AS_DM"

  if [[ $# -gt 1 ]]; then
    msg="$argv[1]"
    shift
  fi

  if [[ $# -gt 1 ]]; then
    st="$argv[1]"
    shift
  fi

  if ! failed "$st" || ! equal "$st" "$argv[1]"; then
    fail "${(e)msg}"
  else
    (( errors-- )) || true #Prevent further error depending on result
  fi
}

function define_checker {
  local i
  local checker
  local name="$argv[1]"; shift         #Name for checker (without 'check' prefix)
  local num_args="$argv[1]"; shift     #Number of arguments the check requires
  local check="$argv[1]"; shift        #Function to call to perform the actual check (accepts ! <function>)
  local msg="$argv[1]"; shift          #Message for failed check

  checker="function check_$name { "
    checker+='local -a args; '
    checker+='local msg="$ZSHUNIT_C_'${name:u}'_DM"; '
    checker+='if [[ $# -gt '$num_args' ]]; then msg="$argv[1]"; shift; fi; '

    checker+='args=('
      for i in {1..$num_args}; do
          checker+=' "$argv['$i']"'
      done
    checker+=' ); '

    checker+='if '$check' "$args[@]"; then fail "${(e)msg}"; fi; '
  checker+=' }'

  def_option "ZSHUNIT_C_${name:u}_DM" "$msg"
  eval "$checker"
}

define_checker 'equal' 2 '! equal' '<$argv[1]> is not equal to <$argv[2]>'
define_checker 'not_equal' 2 'equal' '<$argv[1]> is equal to <$argv[2]>'

define_checker 'empty' 1 '! empty' '<$argv[1]> is not empty'
define_checker 'not_empty' 1 'empty' 'argument is empty'

define_checker 'contains' 2 '! contains' '<$argv[1]> does not contain <$argv[2]>'
define_checker 'not_contains' 2 'contains' '<$argv[1]> contains <$argv[2]>'

define_checker 'array_contains' 2 '! array_contains' '<$argv[1]> does not contain <$argv[2]>'
define_checker 'array_not_contains' 2 'array_contains' '<$argv[1]> contains <$argv[2]>'

define_checker 'same_content' 2 '! same_content' 'content in <$argv[1]> is not equal to content in <$argv[2]>'
define_checker 'not_same_content' 2 'same_content' 'content in <$argv[1]> is equal to content in <$argv[2]>'

define_checker 'exists' 1 '! exists' '<$argv[1]> does not exist'
define_checker 'does_not_exist' 1 'exists' '<$argv[1]> exists'

define_checker 'is_dir' 1 '! is_dir' '<$argv[1]> is not a directory'
define_checker 'is_file' 1 '! is_file' '<$argv[1]> is not a file'
