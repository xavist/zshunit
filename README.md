# ZSHUNIT - xUnit testing for ZSH scripts

## Installing

### Brew

    brew tap xavist/tap https://bitbucket.org/xavist/homebrew-tap
    brew install zshunit

### Manually

Clone the repo and add zshunit to your PATH (*export* the PATH in *~/.zshrc* if you want it to persist)

    git clone --recursive https://xavist@bitbucket.org/xavist/zshunit.git
    cd zshunit
    PATH=$(pwd):$PATH

## Usage

Just run zshunit passing the files that contain your tests:

    zshunit *.tests

If no arguments are passed it's the same as executing:

    zshunit **/*.zsht

## Writing tests and how are they executed

Best thing to do might be to take a look at zshunit own tests, but here's a summary of how everything works.

### Quick test example

    zutest_example() {
      local v1=1
      local v1=2

      mkdir d
      touch f

      check_not_equal "$v1" "$v2"
      check_is_dir d
      check_is_file f
    }

### Suites and tests

Each file passed to zshunit will be considered a *suite*.

On each suite you can define *before* and *after* functions that will be executed before and after each test respectively.

By default, zshunit considers that any function defined in suite starting with *zutest_* is a test.
You can change that by setting the variable *ZSHUNIT_TEST_PREFIX*.

Before loading and executing any suite, zshunit will check if there are any functions that would be executed as tests already defined.
and if that is the case, it will ask if you want to continue or exit.

Each suite and each test, is executed in its own subshell.
Also, zshunit defines a temporary directory for each test and goes into it before executing the test (and the *before* function if exists).
By default those directories and other stuff are created in */tmp/zshunit*. You can change that by setting the variable *ZSHUNIT_TMP_DIR*.

### Tests results: PASSED, FAILED or ERROR

zshunit intends to be very flexible and robust, so it keeps track of both failures and errors and it can report multiple results according to those:

- Any error on the *before* function, will prevent the test and *after* execution and report the result as ERROR.
- Any error on the test, will report ERROR as part of the result.
- Any failure on the test, will report FAILED as part of the result.
- Any error on the *after* function, will report ERROR as part of the result.
- If there aren't any errors or failures the result will be PASSED.

Error is anything the shell considers as such.
Failure is anything that sets the variable *test_status* to *$ZSHUNIT_TEST_FAILED*. You don't need to care about that as long as you use the predefined *checkers*
(take a look at *lib/testing.zshs*) or your own checkers call *fail* when detecting a failure.

If you need to cause errors or check for errors for your test to work properly and pass, make sure you use the *check_failed* or *check_failed_as* checkers
or call the *error_checked* function for each expected error to prevent those from making the test report an ERROR result.

Each test output is stored in a file in *ZSHUNIT_TMP_DIR*, which is only deleted if all the tests passed (exit status 0).
*ZSHUNIT_TMP_DIR* is cleaned up before every zshunit execution though.

### Mocks

You can mock functions (body and return value) and check how many times they were called:

    zutest_mock() {
      my_func() { var1=1; return 1}

      my_func
      check_failed_as 1
      check_equal $var1 1

      mock 'my_func' 'var1=2' 2
      my_func
      check_failed_as 2
      check_equal $var1 2
      check_mock_num_calls 'my_func' 1
    }

## Output

The output will look like this (but with some colors):

    Executing lib/zx/tests/checks.zsht...
    ZX Checks
    Executing zutest_this... [PASSED]
    Executing zutest_empty... [FAILED]
    Check failed: <a> is not empty

    Executing zutest_that... [ERROR]
    Unchecked errors while executing zutest_that

    Executing zutest_more... [PASSED]

    4 tests found: 2 PASSED, 1 FAILED and 1 with ERROR

## Other stuff

Some other default values can be redefined, check the *setup* method in zshunit